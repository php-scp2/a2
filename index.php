<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S2 A1</title>
</head>
<body>

	<h2>DIVISIBLE BY 5</h2>
	<?php modifiedForLoop(); ?>

	<h2>Array Manipulation</h2>

	<?php $students =[]; ?>

   <?php array_push($students, "John Smith"); ?>
   <p><?php var_dump($students); ?></p>

   <p><?= count($students); ?></p>

    <?php array_push($students, "Jane Smith"); ?>
   <p><?php var_dump($students); ?></p>

    <p><?= count($students); ?></p>


	<?php array_shift($students); ?>

	<p><?php var_dump($students); ?></p>

	<p><?= count($students); ?></p>	


</body>
</html>
